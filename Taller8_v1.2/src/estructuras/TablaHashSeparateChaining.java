package estructuras;

import java.util.ArrayList;

public class TablaHashSeparateChaining<K, V>
{
	private int N; // number of key-value pairs

	private int M; // hash table size

	private NodoHash<Integer, ArrayList<NodoHash<K, V>>>[] st; // array of ST objects

	public TablaHashSeparateChaining()
	{ this(997); }
	public TablaHashSeparateChaining(int M)
	{ // Create M linked lists.
		this.M = M;
		st = (NodoHash<Integer, ArrayList<NodoHash<K, V>>>[]) new NodoHash[M];
	}

	private int hash(K key)
	{ 
		return (key.hashCode() & 0x7fffffff) % M;
		}

	public V get(K key)
	{ 
		V v = null;
		ArrayList<NodoHash<K, V>> a = st[hash(key)].getValor();
		for (NodoHash<K, V> nodoHash : a) {
			if (key.equals(nodoHash.getLlave()))
				v = nodoHash.getValor();
		}
		return v;	
	}

	public int put(K key, V val)
	{ 
		int totColis=0;
		ArrayList<NodoHash<K, V>> a;
		{
			a = new ArrayList<NodoHash<K, V>>();
			st[hash(key)] = new NodoHash<Integer, ArrayList<NodoHash<K, V>>>(hash(key), a);
		}
		for (NodoHash<K, V> nodoHash : a) 
		{
			if (key.equals(nodoHash.getLlave()))
			{ nodoHash.setValor(val);return  totColis; } 
		}
		a.add(new NodoHash<K, V>(key, val));
		st[hash(key)].setValor(a);;	

	return totColis;
	}

}