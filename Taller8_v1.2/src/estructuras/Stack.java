package estructuras;

public class Stack<T> {

	private Node<T> top;
	private int size;

	public Stack(){
		this.top = null;
		this.size = 0;
	}


	public T getItemAtTop() {
		if(isEmpty()){
			return null;
		}
		return top.getItem();
	}


	public boolean isEmpty(){
		return top == null;
	}


	public int getSize() {
		return size;
	}


	public void push(T item){
		top = new Node<T>(item, top);
		size++;
	}

	public T pop(){
		T item = null;
		if(top != null){
			item = top.getItem();
			top = top.getNext();
			size--;
		}
		return item;
	}


}



