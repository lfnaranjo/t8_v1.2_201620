package estructuras;
public class RecursiveList<T> {

	private T item;
	private RecursiveList<T> tail;
	
	
	public RecursiveList(){
		this.item = null;
		this.tail = null;
	}


	public RecursiveList(T item, RecursiveList<T> tail) {
		super();
		this.item = item;
		this.tail = tail;
	}
	
	
	
	
	public T getItem() {
		return item;
	}


	public void setItem(T item) {
		this.item = item;
	}


	public RecursiveList<T> getTail() {
		return tail;
	}


	public void setTail(RecursiveList<T> tail) {
		this.tail = tail;
	}


	public void insertAtK(T newItem, int k){
		if(k == 0){			
			if(this.tail != null){
				this.tail = new RecursiveList<T>(this.item, this.tail);
			}
			this.item = newItem;
		}else if (this.tail != null){
			this.tail.insertAtK(newItem, k-1);
		}else{ 
			this.tail = new RecursiveList<T>(newItem,null);
		}
	}
	
}
