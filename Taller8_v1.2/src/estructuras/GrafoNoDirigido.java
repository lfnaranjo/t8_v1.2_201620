package estructuras;



import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Clase que representa un grafo no dirigido con pesos en los arcos.
 * @param K tipo del identificador de los vertices (Comparable)
 * @param E tipo de la informacion asociada a los arcos

 */
public class GrafoNoDirigido<K extends Comparable<K>, E> implements Grafo<K,E> 
{


	/**
	 * Nodos del grafo
	 */
	//TODO Declare la estructura que va a contener los nodos
	private HashMap<K,Nodo> nodos;
	/**
	 * Lista de adyacencia 
	 */
	private HashMap<K, List<Arco<K,E>>> adj;

	private  HashMap<String,Arco<K,E>> arcox;







	//s (defina una representacion para el grafo)
	// Es libre de implementarlo con la representacion de su agrado. 


	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido()
	{

		nodos=new HashMap<K,Nodo>();
		adj=new HashMap<K, List<Arco<K,E>>>();
		arcox=new HashMap<String,Arco<K,E>>();
		//TODO implementar
	}

	@Override
	public boolean agregarNodo(Nodo<K> nodo) 
	{

		nodos.put(nodo.darId(), nodo);
		return true;
	}

	@Override
	public boolean eliminarNodo(K id) 
	{
		//TODO implementar
		return nodos.remove(id)==null;

	}

	@Override
	public Arco<K,E>[] darArcos()
	{

		return (Arco<K, E>[]) arcox.values().toArray();


		//TODO implementar
	}

	private Arco<K,E> crearArco( K inicio, K fin, double costo, E e )
	{


		Nodo<K> nodoI = buscarNodo(inicio);
		Nodo<K> nodoF = buscarNodo(fin);
		if ( nodoI != null && nodoF != null)
		{
			return new Arco<K,E>( nodoI, nodoF, costo, e);
		}
		{
			return null;
		}

	}

	@Override
	public Nodo<K>[] darNodos() 
	{
		//TODO implementa;
		Collection<Nodo>x=nodos.values();
		return (Nodo<K>[]) x.toArray();
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo, E obj) 
	{

		Arco<K,E> temp= crearArco(inicio, fin, costo, obj);

		if(temp==null)
		{
			return false;
		}
		else
		{
			//TODO implementar

			List<Arco<K,E>> templist=adj.get(inicio);

			templist.add(temp);


			List<Arco<K,E>> templist2=adj.get(fin);///no hay que put por que se hace la misma referencia

			templist2.add(temp);
			///cuando flase
			arcox.put(inicio.toString()+fin.toString()+costo+obj.toString(), temp);

			return true;
		}
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo) 
	{
		return agregarArco(inicio, fin, costo, null);
	}

	@Override
	public Arco<K,E> eliminarArco(K inicio, K fin) 
	{
		//TODO implementar

		Arco<K,E> resp=null;
		List<Arco<K,E>> arcos=adj.get(inicio);
		Nodo<K> ini =buscarNodo(inicio);
		Nodo<K> fini =buscarNodo(inicio);
		Iterator<Arco<K,E>> IT=	arcos.iterator();
		boolean encontro=false;
		while(IT.hasNext()&&!encontro)
		{
			Arco<K,E> arc=IT.next();
			if(arc.darNodoFin().equals(fini))
			{ 
				arcos.remove(arc);
				resp=arc;
				arcox.remove(arc);
				encontro=true;
			}

		}
		List<Arco<K,E>> arcos2=adj.get(fin);

		if(!encontro)
		{
			return null;
		}
		else
		{
			encontro=false;	

			IT=	arcos2.iterator();
			while(IT.hasNext()&&!encontro)
			{
				Arco<K,E> arc=IT.next();
				if(arc.darNodoFin().equals(fini))
				{ 
					arcos.remove(arc);
					encontro=true;

				}

			}

			return resp;
		}
	}

	@Override
	public Nodo<K> buscarNodo(K id)
	{

		return nodos.get(id);

		//TODO implementar


	}

	@Override
	public Arco<K,E>[] darArcosOrigen(K id) 
	{


		List<Arco<K,E>> arcos=adj.get(id);
		Arco<K,E>[] resp= (Arco<K, E>[]) new Object[arcos.size()/2];
		int i=0;

		Nodo current=buscarNodo(id);


		Iterator<Arco<K,E>> IT=	arcos.iterator();
		while(IT.hasNext())
		{
			Arco<K,E> arc=IT.next();
			if(arc.darNodoInicio().equals(current))
			{ 
				resp[i]=arc;
				i++;
			}


		}

		//TODO implementar

		return resp;
	}

	@Override
	public Arco<K,E>[] darArcosDestino(K id) 
	{



		List<Arco<K,E>> arcos=adj.get(id);
		Arco<K,E>[] resp= (Arco<K, E>[]) new Object[arcos.size()/2];
		int i=0;

		Nodo current=buscarNodo(id);


		Iterator<Arco<K,E>> IT=	arcos.iterator();
		while(IT.hasNext())
		{
			Arco<K,E> arc=IT.next();
			if(arc.darNodoFin().equals(current))
			{ 
				resp[i]=arc;
				i++;
			}


		}

		//TODO implementar

		return resp;

		//TODO implementar
	}

	
	


	
	
}
