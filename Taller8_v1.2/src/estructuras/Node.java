package estructuras;

public class Node <T>{

	private T item;
	private Node<T> next;


	public Node() {

		this.item = null;
		this.next = null;
	}


	public Node(T item, Node<T> next) {
		super();
		this.item = item;
		this.next = next;
	}

	public T get(int pos)
	{
		if(pos == 0 )
			return item;
		else if (next == null) 
			return null;
		else 
			return next.get(pos-1);

	}


	public T getItem() {
		return item;
	}
	public void setItem(T item) {
		this.item = item;
	}
	public Node<T> getNext() {
		return next;
	}
	public void setNext(Node<T> next) {
		this.next = next;
	}

}
