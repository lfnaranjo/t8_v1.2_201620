package estructuras;


public class Queue<T> {

	private Node<T> first;
	private Node<T> last;
	private int size; 
	
	public Queue(){
		this.first = null;
		this.last = null;
		this.size = 0 ;
	}
	
	
	
	public T dequeue(){
		T item = null;
		if(!isEmpty()){
			item  = first.getItem();
			first = first.getNext();
			size--;
		}
		
		if(isEmpty()) last = null;
		return item;
	}
	
	public void enqueue(T item){
		Node<T> oldLast = this.last;
		last = new Node<T>(item, null);
		if(isEmpty()){
			first = last;
		}else{
			oldLast.setNext(last);
		}
		size++;
	}
	
	public boolean isEmpty(){
		return first == null;
	}
	
	
	public int getSize() {
		return size;
	}
	

	
}
