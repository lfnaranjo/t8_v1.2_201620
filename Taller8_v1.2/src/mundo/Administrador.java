package mundo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Set;
import java.util.TreeSet;

import modelos.Estacion;
import estructuras.Arco;
import estructuras.Grafo;
import estructuras.GrafoNoDirigido;
import estructuras.Nodo;

/**
 * Clase que representa el administrador del sistema de metro de New York
 * @param <K>
 * @param <E>
 *
 */
public class Administrador<K extends Comparable<K>, E>


{

	/**
	 * Ruta del archivo de estaciones
	 */
	public static final String RUTA_PARADEROS ="./data/stations.txt";

	/**
	 * Ruta del archivo de rutas
	 */
	public static final String RUTA_RUTAS ="./data/routes.txt";


	

	/**
	 * Grafo que modela el sistema de metro de New York
	 */
	//TODO Declare el atributo grafo No dirigido que va a modelar el sistema. 
	// Los identificadores de las estaciones son String
	private GrafoNoDirigido<String, String>  graph;

	/**
	 * Construye un nuevo administrador del Sistema
	 */
	public Administrador() 
	{
		graph= new GrafoNoDirigido<String,String>();


		//TODO inicialice el grafo como un GrafoNoDirigido
	}

	/**
	 * Devuelve todas las rutas que pasan por la estacion con nombre dado
	 * @param identificador 
	 * @return Arreglo con los identificadores de las rutas que pasan por la estacion requerida
	 */
	public String[] darRutasEstacion(String identificador)
	{
		//TODO Implementar]
		Arco<String, String>[] x=graph.darArcosOrigen(identificador);
		String[] resp=new String[x.length];
		for(int i=0;i<x.length;i++)
		{
			resp[i]=x[i].darInformacion();
			
		}
		return resp;
	}

	/**
	 * Devuelve la distancia que hay entre las estaciones mas cercanas del sistema.
	 * @return distancia minima entre 2 estaciones
	 */
	public double distanciaMinimaEstaciones()
	{
		double dist=Double.POSITIVE_INFINITY;
	Arco<String,String>[] x=graph.darArcos();
	for(int i=0; i<x.length;i++)
	{
		if(x[i].darCosto()<dist)
		dist=x[i].darCosto();
		
	}
	return dist;
		
	
	}

	/**
	 * Devuelve la distancia que hay entre las estaciones más lejanas del sistema.
	 * @return distancia maxima entre 2 paraderos
	 */
	public double distanciaMaximaEstaciones()
	{
		double dist=0;
		Arco<String,String>[] x=graph.darArcos();
		for(int i=0; i<x.length;i++)
		{
			if(x[i].darCosto()>dist)
			dist=x[i].darCosto();
			
		}
		return dist;
			
	}


	/**
	 * Metodo encargado de extraer la informacion de los archivos y llenar el grafo 
	 * @throws Exception si ocurren problemas con la lectura de los archivos o si los archivos no cumplen con el formato especificado
	 */
	public void cargarInformacion() throws Exception
	{
		//TODO Implementar

		String cadena;
		FileReader f = new FileReader(RUTA_PARADEROS);
		BufferedReader b = new BufferedReader(f);
		while((cadena = b.readLine())!=null)
		{

			int numEstaciones=Integer.parseInt(b.readLine());
			for(int i=0; i<numEstaciones;i++)
			{

				String[] estastring=	b.readLine().split(";");
				Estacion<String> temp=new Estacion<String>(estastring[0],Double.valueOf(estastring[1]),Double.valueOf(estastring[2]));
				graph.agregarNodo( temp);
			}

		}
		b.close();

		System.out.println("Se han cargado correctamente "+graph.darNodos().length+" Estaciones");

		//TODO Implementar



		f = new FileReader(RUTA_RUTAS);
		b = new BufferedReader(f);
		while((cadena = b.readLine())!=null)
		{
			if(cadena.equals("--------------------------------------"))
			{
				String sevicio=b.readLine();
				int numeroestaciones=Integer.parseInt(b.readLine());
				String[] inicios=new String[numeroestaciones];
				String[] finales=new String[numeroestaciones];
				Double[] costo=new Double[numeroestaciones];

				for(int i=0;i<numeroestaciones;i++)
				{


					String[] temp=b.readLine().split(" ");
					inicios[i]=temp[0];
					costo[i]=Double.valueOf(temp[1]);
					if(i!=0)
						finales[i-1]=temp[0];
				}

				for(int i=0;i<numeroestaciones;i++)
				{
					graph.agregarArco(inicios[i], finales[i], costo[i],sevicio);

				}


			}
		}
		b.close();

		System.out.println("Se han cargado correctamente "+ graph.darArcos().length+" arcos");
	}

	/**
	 * Busca la estacion con identificador dado<br>
	 * @param identificador de la estacion buscada
	 * @return estacion cuyo identificador coincide con el parametro, null de lo contrario
	 */
	public Estacion<String> buscarEstacion(String identificador)
	{
		//TODO Implementar
	return	(Estacion<String>) graph.buscarNodo(identificador);

	}






}

